﻿using UnityEngine;
using System.Collections;

public class ProjectileButton : MonoBehaviour {
    public GameObject projectilePrefab;
    Transform projectileTransform;
    public ProjectileBehaviour.ProjectileType type;
    bool isSpawned;
    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (projectileTransform != null)
            {
                Vector3 pos = Input.mousePosition;
                pos.z = -Camera.main.transform.position.z;
                projectileTransform.position = Camera.main.ScreenToWorldPoint(pos);
            }
            else
            {
                GameObject projectile = Instantiate<GameObject>(projectilePrefab);
                projectileTransform = projectile.transform;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 pos = Input.mousePosition;
            pos.z = -Camera.main.transform.position.z;
            ProjectileBehaviour.TriggerProjectile(type, pos);
        }
    }
}
